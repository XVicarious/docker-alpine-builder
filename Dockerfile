FROM alpine:3.9
MAINTAINER Brian Maurer aka XVicarious
RUN apk --no-cache add build-base git upx ca-certificates
